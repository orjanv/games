from turtle import *

def drawTriangle(size, _color):
    color(_color)
    pendown()
    begin_fill()
    for line in range(3):
        left(120)
        forward(size)
    right(120)
    end_fill()
    penup()


def main():
    setup(1200, 700)
    speed(0)
    bgcolor("Black")
    #hideturtle()

    for i in range(50):
        drawTriangle(10, "White")


if __name__ == '__main__':
    main()

