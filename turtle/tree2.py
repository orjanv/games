import turtle
import random

def tree(branchLen,t):
    rLen = random.randint(5, 15)
    rAngle = random.randint(15, 40)
    t.color("brown")
    t.width(2)
    
    # If we have reached the tip of the branches, paint them green
    if branchLen < 10:
        t.color("green")
        t.width(3)
        
    # Main recursive algorithm that builds the tree
    if branchLen > 5:
        t.width(2)
        t.forward(branchLen)
        t.left(rAngle)
        tree(branchLen-rLen,t)
        t.right(rAngle*2)
        tree(branchLen-rLen,t)
        t.left(rAngle)
        t.backward(branchLen)
        t.color("brown")
        t.width(2)


def main():
    # Setup and initialize the canvas
    t = turtle.Turtle()
    s = turtle.Screen()
    s.setup(1.0, 1.0)
    t.hideturtle()
    t.left(90)
    t.up()
    t.backward(350)
    t.down()
    t.speed(0)
    treeSize = random.randint(50, 110)

    # Start building the tree
    tree(treeSize,t)

    # Save canvas as .eps file
    ts = t.getscreen()
    canvas = ts.getcanvas()
    canvas.postscript(file="tree-%s.eps", colormode = 'color')

    s.exitonclick()

main()
